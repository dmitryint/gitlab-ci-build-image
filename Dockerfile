FROM risingstack/alpine:3.4-v4.4.7-3.6.2

ENV DOCKER_BUCKET get.docker.com
ENV DOCKER_VERSION 1.12.0
ENV DOCKER_SHA256 3dd07f65ea4a7b4c8829f311ab0213bca9ac551b5b24706f3e79a97e22097f8b

RUN apk -U add --no-cache \
	curl \
	python \
	python3 \
	python3-dev \
	py-virtualenv \
	ca-certificates \
	postgresql-dev \
	libxslt \
	cairo \
	glib \
	pango \
	jpeg \
	openssh \
	py-pip \
	build-base \
	git \
	bash \
	python-dev \
	py-virtualenv \
	libxml2-dev \
	libxslt-dev \
	libjpeg-turbo-dev \
	libffi-dev

RUN mkdir -p /etc/ssl/certs \
	&& update-ca-certificates

RUN set -x \
	&& curl -fSL "https://${DOCKER_BUCKET}/builds/Linux/x86_64/docker-${DOCKER_VERSION}.tgz" -o docker.tgz \
	&& echo "${DOCKER_SHA256} *docker.tgz" | sha256sum -c - \
	&& tar -xzvf docker.tgz \
	&& mv docker/* /usr/local/bin/ \
	&& rmdir docker \
	&& rm docker.tgz \
	&& docker -v

ENV NODE_ENV ""

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["sh"]
